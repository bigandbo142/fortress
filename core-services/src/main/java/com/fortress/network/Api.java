package com.fortress.network;

import com.fortress.network.models.LoginResponse;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by hieunt on 4/5/17.
 */

public interface Api {

    @FormUrlEncoded
    @POST("kindergarten/login")
    @Headers({
            "Back-Door: f154dc20e2c126636e8c3da61d15106c"
    })
    Observable<LoginResponse> doLogin(
            @Field("kindergarten_id") int pKinderGartenId,
            @Field("username") String pUsername,
            @Field("password") String pPassword,
            @Field("device_token") String pToken,
            @Field("device_type") int pType
    );

}
