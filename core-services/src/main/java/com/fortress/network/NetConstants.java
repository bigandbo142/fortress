package com.fortress.network;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by hieunt on 4/11/17.
 */

public class NetConstants {
    public static final int CACHE_SIZE = 10 * 1024 * 1024;
    public static final int CACHE_MAX_AGE = 120; // seconds
    public static final int CACHE_MAX_STALE = 1; // day
    public static final int CONNECT_TIMEOUT = 15; // seconds
    public static final int READ_TIMEOUT = 15; // seconds
    public static final int WRITE_TIMEOUT = 15; // seconds
    public static final int API_RETRY_COUNT = 3;

//    public static final int ERROR_NETWORK = 0;
//    public static final int ERROR_HTTP = 1;
//    public static final int ERROR_UNEXPECTED = 2;

//    @IntDef({ERROR_NETWORK, ERROR_HTTP, ERROR_UNEXPECTED})
//    @Retention(RetentionPolicy.SOURCE)
//    public @interface ErrorKind{}

    /** Identifies the event kind which triggered a . */
    public enum Kind {
        /** An {@link IOException} occurred while communicating to the server. */
        NETWORK,
        /** A non-200 HTTP status code was received from the server. */
        HTTP,
        /**
         * An internal error occurred while attempting to execute a request. It is best practice to
         * re-throw this exception so your application crashes.
         */
        UNEXPECTED
    }
}
