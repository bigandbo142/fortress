package com.fortress;

import com.fortress.network.Api;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.OkHttpClient;

@Singleton
public class NetServiceProvider {
    private static final String TAG = "NetServiceProvider";

    private Api mApi;
    private OkHttpClient mOkHttpClient;

    public String getTag(){
        return TAG;
    }

    @Inject
    NetServiceProvider(Api pApi, OkHttpClient okHttpClient){
        this.mApi = pApi;
        this.mOkHttpClient = okHttpClient;
    }

    public Api getApiService() {
        return mApi;
    }

    public OkHttpClient getOkHttpClient() {
        return mOkHttpClient;
    }

}
