package com.fortress.persistent;

import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.fortress.persistent.PersistentConstants.SP_KEY_TOKEN;
import static com.fortress.persistent.PersistentConstants.SP_KEY_USERNAME;

/**
 * Created by hieunt on 4/12/17.
 */

@Singleton
public class PersistentController {

    private SharedPreferences mPreferences;

    @Inject
    public PersistentController(SharedPreferences pPreferences) {
        this.mPreferences = pPreferences;
    }

    public void saveLoginSession(String pUserName, String pToken){
        mPreferences.edit()
                .putString(SP_KEY_USERNAME, pUserName)
                .putString(SP_KEY_TOKEN, pToken)
                .apply();
    }

    public Map<String, String> getLoginSession(){
        Map<String, String> loginSession = new HashMap<>();

        loginSession.put(SP_KEY_USERNAME, mPreferences.getString(SP_KEY_USERNAME, ""));
        loginSession.put(SP_KEY_TOKEN, mPreferences.getString(SP_KEY_TOKEN, ""));

        return loginSession;
    }

}
