package com.fortress.persistent.di.modules;

import android.content.Context;
import android.content.SharedPreferences;

import com.fortress.persistent.PersistentConstants;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hieunt on 4/12/17.
 */

@Module
public class PersistentModule {

    @Provides
    @Singleton
    public SharedPreferences provideSharedPrefs(@Named("application_ctx") Context pContext){
        return pContext.getSharedPreferences(PersistentConstants.SP_NAME, Context.MODE_PRIVATE);
    }
}
