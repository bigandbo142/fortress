package com.fortress.persistent;

import android.content.SharedPreferences;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by hieunt on 4/12/17.
 */

@Singleton
public class PersistentServiceProvider {
    private static final String TAG = "PersistentServiceProvider";

    private SharedPreferences mPreferences;
    private PersistentController mPersistentController;

    @Inject
    public PersistentServiceProvider(SharedPreferences pPreferences, PersistentController pPersistentController) {
        this.mPreferences = pPreferences;
        this.mPersistentController = pPersistentController;
    }

    public String getTag() {
        return TAG;
    }

    public SharedPreferences getPreferences() {
        return mPreferences;
    }

    public PersistentController getPersistentController() {
        return mPersistentController;
    }
}
