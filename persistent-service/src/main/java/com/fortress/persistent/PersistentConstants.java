package com.fortress.persistent;

/**
 * Created by hieunt on 4/12/17.
 */

public class PersistentConstants {
    public final static String SP_NAME = "sp_app";
    public final static String SP_KEY_USERNAME = "sp_key_username";
    public final static String SP_KEY_TOKEN = "sp_key_token";
}
