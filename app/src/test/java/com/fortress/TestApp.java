package com.fortress;

import android.app.Application;

import com.fortress.commons.App;
import com.fortress.di.components.AppComponent;
import com.fortress.di.components.DaggerTestAppComponent;
import com.fortress.di.components.TestAppComponent;
import com.fortress.di.modules.TestAndroidModule;

/**
 * Created by hieunt on 4/27/17.
 */

public class TestApp extends App{


    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = initTestAppComponent();

        ((TestAppComponent)mComponent).inject(this);
    }

    public TestAppComponent initTestAppComponent() {
        return DaggerTestAppComponent.builder()
                .testAndroidModule(new TestAndroidModule(this))
                .build();
    }


    public static TestAppComponent getTestComponent(){
        return (TestAppComponent) mComponent;
    }
}
