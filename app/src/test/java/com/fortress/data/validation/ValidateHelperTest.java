package com.fortress.data.validation;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import static org.junit.Assert.*;

import static org.hamcrest.Matchers.*;

/**
 * Created by hieunt on 4/20/17.
 */

public class ValidateHelperTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks ValidateHelper mValidateHelper;

    @Before
    public void setUp(){

    }

    @Test
    public void validateUsernameShouldSuccess(){
        String fakeUsername = "Skywander";

        assertTrue("Username\'s must be between 6 and 32 characters", mValidateHelper.validateUsername(fakeUsername));

    }

    @Test
    public void validateUsernameShouldFailed() throws Exception{
        String fakeUsernameEmpty = "";
        String fakeUsernameShort = "Hieu";
        String fakeUsernameLong = "Hieu123456789012345678901234567890";

        assertFalse("Username\'s cannot be null", mValidateHelper.validateUsername(null));
        assertFalse("Username\'s cannot be empty", mValidateHelper.validateUsername(fakeUsernameEmpty));
        assertFalse("Username\'s length cannot be less than 6 characters", mValidateHelper.validateUsername(fakeUsernameShort));
        assertFalse("Username\'s length cannot be greater than 32 characters", mValidateHelper.validateUsername(fakeUsernameLong));

    }

    @Test
    public void validatePasswordShouldSuccess(){
        String fakeLegalPassword = "123456";

        assertTrue("Password\'s length must be between 6 and 255 characters", mValidateHelper.validatePassword(fakeLegalPassword));
    }

    @Test
    public void validatePasswordShouldFailed(){
        String fakePasswordShort = "12345";
        String fakePasswordEmpty = "";
        String fakePasswordLong = "RT4P7H6tZ7c47cqyJhJnVDeJ8uCnInVZks4u3stOkgUz9vcnV1X0iFa4PBaP6FVJEwMybRlsg5JYHQVbCzPfQuLQZR0vHhDeP2hHQC03wKgu2zlOQ00H7Geh0sHZHhgoMum1xiklI7CYOA747PByRrVRRwAffLonaetoz3kmXqKDOjulUi3ekyXJi3JSfS4FVVZK8fEVN4zw5qBXKHIJf8DlVR1xoSpIbbhokDnJs3yxDUbYZvXQVTfhWqv38UhDvxFep6rrO6V9iMwM1BaYRKq0z0HYvzRNsKEftNFBk5l9CL7fDRyElga75HyjazOtjZBpa91R5Zj32VHVIblCaGNWlxnfJAqt6mAqJ8UE8iu8cSIoWrUxLPmVVGVMPzg3CXs6bBwCrHZWNMCHSIWTbDEnPRuTfjNQo8SqAhnLCQP8hAm7pHPhl48kEoTnVlO7qlf8wmxGfgrh6OYV72Bt5eqgjlfeuOwnJWqDxrtNB1ueecxXpch3zePseQpTCseC6BxInZLHtvb5QPKHjuVrn7Nh6YYlkm4x65pD3ZfevfnDoVjY9lYFkmpSo0JE5DWvSA85jfe1UHvcRDg2yHDvbsJfGfW81syJkjQ3T0kCUWH4DiP1z9VTZtv4ormr5kziDhHfvzoVJLTJpYPwbUPszH4Afsfou2v8wRMSfskPhffCCPfwRUGbvWOyWYSiDJDGBejIUqSvlqqjahC3Ct9ncz35iiN42Yor3km3wj8vD8PUFYFCeuHxtGi6sVzmgGPFqD0inJSJ6JAFsfPbXiylhJ3guk13mGyI1RrZl9PnRDTz0hkEzKq90f843wVpXr7guPrfGhNB3Prp45Dv5ICYRqtHSWN0cBzTOXUwlMHOCVCAbVRjZgvhRtMcAkj8uT262qLyJ7wWqWeTDpDlzNRCf6O31iLhaOoQraf4WAxsnFpY3tO4bP58CpWKDVl3aVTKcMzY0imBXOcVXc9EZT9tZL7IgOkRKYwR0qRluo4bH5N4GpBtZG5mT8lDhyrh7vRXHzOwsJFMuYpZkBVJ3qBJ7tsENYHOpp7joelht9DGAEBRikZbTiHKIfJ3KzxagiaRfN2KXbeTxGQyKC3ijuEepD7Sa2NgjjUjLz3ak5ZzlVr0y8hyogsfHWGmxs8wax3sBQs5EqCtfMOFB2rrfUbMneFreBQq4o67rwzZekKpxgftKTbU5KVOjRYXq7ayhAqWXL49bupWGpLSzms2iKDrMuV5f2CYX6YZ3BrzyDHPSOUbHfUnekhhKX9srKtJFAmYW4BS1Qin8CHaX5fluKZeKDIN5R4MrPRsbykshnG1DVDuvu4vo5DBRLQFqr5yCbBYs0RLvxZV5lcl2Itp03JPLqy8eYZeZthSP893l50b6YQrojQrvm6qWrBWsqw7xeaSB2UPImqeiE5NlVtFiphcQCc7W5XCmuQ2N4tvyPomfW60s8SPr2Nf0vyjrqofXPEnYLFR8lANskwWYgJhxEhYqfMQaZekP2158RxPpJXxTccqBgFxC0EKjvRrsPROLVcBraEwffbc9UnUUzlwUp9ZlsyJMnqubP1KyLV8F3TacDHcXONMz0ZvDI6pZ3kcjmqxJ0DxRmUbe77KjtLeZ82PX2wSgaQcmZ2qtf8k5D4emzsFXHTvmxPOXcXMpCOp3ahW77aeE0l8aL9J78X5gBN72flxgEDXJzGfDR8PQhUzYGUpyj2keCm4MFBAYcCVfP5r5xp9uJroXybqJ3w6iem3UbilAIWDzYfy2vimgePItlqmkMcl1iMunh6pQQC9p3EHuLwLvu1rkb0YK72kotonB2WQQXKcimLFsJaIEAljGmK5bwsAPvrfRKQCtF8iNLli7888yzKEagtgp2vzXM9ZLwc5h8SUT2thzOjW7gyXTq2BaMnLBtDWuemAI0aXntpWvIViw22VUXeEcqv6RvgRAKyjgBKokRAKxB7hvG88PHHuZhlSI7kqRX0fPgApBQ6s3T375DMiZcEBC2rFypZXM4mbSoc8tifzSD73vsXjZ2FURD5y0JIpou2OmkHSelJtyy16xniewQtNQSoiMgzxAH2QPtwZzQHUFhbN0swBM61rEwxQrrJ9cQlT2FRovl2aM6GI3tOy61fJGOyC6lKApk3OrQSl4prZqJtKWr0L8eO1hZQ0kKrIpz2QSJ9JYQCw0HKDwbHhW8n7xVV5avkgIATux0y4Fhjvk9swHHUwk7eqXXOflmZLkRW3lgpXHGbx48UT4jmhOaabfwKXrI45s6Gk66XEJMiPoo2ONMiq9NjibmYklwYzf7jArDx3jhQYzCGP6XH3u0s24NPyMxw00iFuyZa4ebaHm66uZZxjB5gDx0g7vo4G64lrRUS87uqnXR9LtN3aIVLIOtD7gZkVZOO5Si2vxhCO47gO1imYOMyz2JonVFOzREwL5iNy9aIiO1hatLtQbLYZ4W42IJZZSuUJPSSpyRGyJ4QyX6jbSisn4SuDYLZ00WEsUeS1g2Z9sNcftNpE4GjJCOuHWD2bMfTMJh7EB9HkxUbN0KUhP1qOq31DQGUTuz";
        assertFalse("Password\'s length cannot be less than 6 characters", mValidateHelper.validatePassword(fakePasswordShort));
        assertFalse("Password\'s length cannot be greater than 255 characters", mValidateHelper.validatePassword(fakePasswordLong));
        assertFalse("Password cannot be empty", mValidateHelper.validatePassword(fakePasswordEmpty));
        assertFalse("Password cannot be null", mValidateHelper.validatePassword(null));
    }

}
