package com.fortress.data.local;

import android.content.Context;
import android.content.SharedPreferences;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.*;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.contains;


/**
 * Created by hieunt on 4/20/17.
 */

@RunWith(MockitoJUnitRunner.class)
public class PreferencesHelperTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Rule
    public ExpectedException thrown= ExpectedException.none();


    @Mock Context context;
    @Mock SharedPreferences sharedPreferences;
    @Mock SharedPreferences.Editor editor;
    @InjectMocks PreferencesHelper prefsHelper;



    @Before
    public void setUp(){
        context = Mockito.mock(Context.class);
        Mockito.when(editor.putString(anyString(), anyString())).thenReturn(editor);
    }

    @Test
    public void storeLoginSessionShouldBeSuccess(){
        assertNotNull("PreferenceHelper must be not null",prefsHelper);
        assertNotNull("Context must be not null",context);

        assertNotNull(sharedPreferences);

        assertNotNull("Preference must be not null", prefsHelper.getPref());
        assertNotNull("Preference editor must be not null", prefsHelper.getEditor());

        String fakeUsername = "test_account";
        String fakeToken = "test_token";

        assertTrue(prefsHelper.storeLoginSession(fakeUsername, fakeToken));
    }

    @Test
    public void storeLoginSessionShouldReturnFalseWhenException(){
        Mockito.when(editor.putString(anyString(), anyString())).thenReturn(null);

        String fakeUsername = "test_account";
        String fakeToken = "test_token";

        assertFalse(prefsHelper.storeLoginSession(fakeUsername, fakeToken));
    }

    @Test
    public void loadLoginSessionShouldBeSuccess(){
        String fakeUsername = "test_account";
        String fakeToken = "test_token";
        Mockito.when(prefsHelper.getPref().getString(contains(PreferencesHelper.PREF_KEY_USERNAME), anyString())).thenReturn(fakeUsername);
        Mockito.when(prefsHelper.getPref().getString(contains(PreferencesHelper.PREF_KEY_TOKEN), anyString())).thenReturn(fakeToken);

        assertNotNull(prefsHelper.loadLoginSession());

        assertEquals(prefsHelper.loadLoginSession().get(PreferencesHelper.PREF_KEY_USERNAME), fakeUsername);
        assertEquals(prefsHelper.loadLoginSession().get(PreferencesHelper.PREF_KEY_TOKEN), fakeToken);
    }

    @Test
    public void loadLoginSessionShouldReturnNullWhenNoSession(){
        String fakeUsernameEmpty = "";
        String fakeTokenEmpty = "";

        Mockito.when(prefsHelper.getPref().getString(contains(PreferencesHelper.PREF_KEY_USERNAME), anyString())).thenReturn(fakeUsernameEmpty);
        Mockito.when(prefsHelper.getPref().getString(contains(PreferencesHelper.PREF_KEY_TOKEN), anyString())).thenReturn(fakeTokenEmpty);

        assertNull(prefsHelper.loadLoginSession());
    }

    @Test
    public void loadLoginSessionExpectException(){
        Mockito.when(prefsHelper.getPref().getString(contains(PreferencesHelper.PREF_KEY_USERNAME), anyString())).thenReturn(null);

        assertNull(prefsHelper.loadLoginSession());

        Mockito.when(prefsHelper.getPref().getString(contains(PreferencesHelper.PREF_KEY_TOKEN), anyString())).thenReturn(null);

        assertNull(prefsHelper.loadLoginSession());
    }



    @Test
    public void testClearShouldSuccess(){
        Mockito.when(prefsHelper.getEditor().clear()).thenReturn(prefsHelper.getEditor());

        assertTrue(prefsHelper.clear());
    }

    @Test
    public void testClearShouldReturnFalseWhenException(){
        Mockito.when(prefsHelper.getEditor().clear()).thenReturn(null);

        assertFalse(prefsHelper.clear());
    }
}
