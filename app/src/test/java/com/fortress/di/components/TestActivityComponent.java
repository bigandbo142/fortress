package com.fortress.di.components;

import com.fortress.di.scopes.PerActivity;
import com.fortress.features.auth.LoginActivityPresenterTest;

import dagger.Component;
import dagger.Subcomponent;

/**
 * Created by hieunt on 4/28/17.
 */

@PerActivity
@Subcomponent
public interface TestActivityComponent {
    void inject (LoginActivityPresenterTest test);
}
