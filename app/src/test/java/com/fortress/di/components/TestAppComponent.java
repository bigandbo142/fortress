package com.fortress.di.components;

import com.fortress.TestApp;
import com.fortress.di.modules.TestAndroidModule;
import com.fortress.di.modules.TestAppModule;
import com.fortress.di.modules.TestDataModule;
import com.fortress.features.auth.LoginActivityPresenterTest;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by hieunt on 4/27/17.
 */

@Singleton
@Component(modules = {
        TestAppModule.class,
        TestAndroidModule.class,
        TestDataModule.class
})
public interface TestAppComponent extends AppComponent{

    TestActivityComponent plus();

    void inject (TestApp testApp);
}
