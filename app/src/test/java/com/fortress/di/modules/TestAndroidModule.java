package com.fortress.di.modules;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;

import com.fortress.TestApp;
import com.fortress.commons.App;

import org.mockito.Mockito;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hieunt on 4/27/17.
 */

@Module
public class TestAndroidModule {
    private TestApp mApplication;

    public TestAndroidModule(TestApp pApplication) {
        this.mApplication = pApplication;
    }

    @Provides
    @Singleton
    public Context provideContext(){

        App context = Mockito.mock(App.class);
        Mockito.when(context.getApplicationContext()).thenReturn(context);
        Mockito.when(context.getComponent()).thenReturn(mApplication.getComponent());

        return context;
    }

    @Provides
    @Singleton
    public Resources provideResources(){
        return Mockito.mock(Resources.class);
    }

    @Provides
    @Singleton
    public SharedPreferences provideSharePreferences(){
        return Mockito.mock(SharedPreferences.class);
    }

    @Provides
    @Singleton
    SharedPreferences.Editor provideEditor(){
        return Mockito.mock(SharedPreferences.Editor.class);
    }
}
