package com.fortress.di.modules;

import com.fortress.data.remote.ApiHelper;
import com.fortress.data.remote.IApiHelper;
import com.fortress.network.Api;

import org.mockito.InjectMocks;
import org.mockito.Mockito;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hieunt on 4/27/17.
 */

@Module
public class TestDataModule {

    @Singleton
    @Provides
    public IApiHelper provideApiHelper(){
        Api api = Mockito.mock(Api.class);
        return new ApiHelper(api);
    }
}
