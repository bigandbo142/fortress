package com.fortress.features.auth;

import com.fortress.TestApp;
import com.fortress.di.components.DaggerTestAppComponent;
import com.fortress.di.components.TestAppComponent;
import com.fortress.di.modules.TestAndroidModule;
import com.fortress.network.models.LoginResponse;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import javax.inject.Inject;

import rx.Observable;

import static org.junit.Assert.*;

/**
 * Created by hieunt on 4/28/17.
 */

public class LoginActivityPresenterTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock TestApp testApp;
    @Mock LoginActivityContract.LoginView mView;
    @Inject LoginActivityPresenter mPresenter;

    TestAppComponent testAppComponent;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        testAppComponent = DaggerTestAppComponent.builder().testAndroidModule(new TestAndroidModule(testApp)).build();

        testAppComponent.plus().inject(this);
        mPresenter.registerView(mView);
    }

    @Test
    public void login_NormalInput_ReturnSuccess(){
        String fakeRightUsername = "22222";
        String fakeRightPassword = "123456";

        Mockito.when(mPresenter.getDataManager().getApiHelper().doLogin(fakeRightUsername,fakeRightPassword)).thenReturn(Observable.just(new LoginResponse()));

        mPresenter.doLoginWithInput(fakeRightUsername, fakeRightPassword);

        Mockito.verify(mView).toggleLoading(true);
        Mockito.verify(mView).toggleLoading(false);
        Mockito.verify(mView).loginSuccess();
        Mockito.verify(mView, Mockito.never()).loginFailed(Matchers.anyString());
    }

    @Test
    public void login_InputWrongUsername_ReturnFailed(){
        String fakeWrongUsername = "123456";
        String fakeRightPassword = "123456";
        Mockito.when(mPresenter.getDataManager().getApiHelper().doLogin(fakeWrongUsername,fakeRightPassword)).thenReturn(Observable.error(new Throwable()));

        mPresenter.doLoginWithInput(fakeWrongUsername, fakeRightPassword);

        Mockito.verify(mView).toggleLoading(true);
        Mockito.verify(mView).toggleLoading(false);
        Mockito.verify(mView, Mockito.never()).loginSuccess();
        Mockito.verify(mView).loginFailed(Matchers.anyString());
    }

    @Test
    public void login_InputWrongPassword_ReturnFailed(){
        String fakeRightUsername = "22222";
        String fakeWrongPassword = "1234567";
        Mockito.when(mPresenter.getDataManager().getApiHelper().doLogin(fakeRightUsername,fakeWrongPassword)).thenReturn(Observable.error(new Throwable()));

        mPresenter.doLoginWithInput(fakeRightUsername, fakeWrongPassword);

        Mockito.verify(mView).toggleLoading(true);
        Mockito.verify(mView).toggleLoading(false);
        Mockito.verify(mView, Mockito.never()).loginSuccess();
        Mockito.verify(mView).loginFailed(Matchers.anyString());
    }

    @Test
    public void login_InputWrongAll_ReturnFailed(){
        String fakeRightUsername = "hhfejksfhsf";
        String fakeWrongPassword = "sgjseghseg";
        Mockito.when(mPresenter.getDataManager().getApiHelper().doLogin(fakeRightUsername,fakeWrongPassword)).thenReturn(Observable.error(new Throwable()));

        mPresenter.doLoginWithInput(fakeRightUsername, fakeWrongPassword);

        Mockito.verify(mView).toggleLoading(true);
        Mockito.verify(mView).toggleLoading(false);
        Mockito.verify(mView, Mockito.never()).loginSuccess();
        Mockito.verify(mView).loginFailed(Matchers.anyString());
    }

    @Test
    public void validateInput_Right_ReturnSuccess(){
        String fakeRightUsername = "22222";
        String fakeWRightPassword = "123456";

        assertTrue(mPresenter.doValidateInput(fakeRightUsername, fakeWRightPassword));
    }

    @Test
    public void validateInput_InvalidUsername_ReturnFailed(){
        String fakeShortUsername = "123";
        String fakeLongUsername = "12395384748yeshgsegkjhsekgsehgwyrwhsekgjehsgjshgjskeghsegskghseg";
        String fakeRightPassword = "123456";

        assertFalse("Username is too short",mPresenter.doValidateInput(fakeShortUsername, fakeRightPassword));

        assertFalse("Username is too long",mPresenter.doValidateInput(fakeLongUsername, fakeRightPassword));
    }

    @Test
    public void validateInput_InvalidPassword_ReturnFailed(){
        String fakeRightUsername = "22222";
        String fakeShortPassword = "123";

        assertFalse("Password is too short",mPresenter.doValidateInput(fakeRightUsername, fakeShortPassword));
    }

}
