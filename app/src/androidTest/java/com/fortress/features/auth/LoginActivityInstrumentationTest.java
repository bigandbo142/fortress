package com.fortress.features.auth;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import com.fortress.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Locale;

import static org.hamcrest.Matchers.not;

/**
 * Created by hieunt on 5/3/17.
 */

@RunWith(AndroidJUnit4.class)
public class LoginActivityInstrumentationTest {

    @Rule
    public ActivityTestRule<LoginActivity> activityTestRule = new ActivityTestRule<>(LoginActivity.class);

    private void setLocaleTo(Locale newLocale) {
        Context context = InstrumentationRegistry.getTargetContext();
        Locale locale = newLocale;
        Locale.setDefault(locale);
        Resources resources = context.getResources();
        Configuration configuration = resources.getConfiguration();
        if (android.os.Build.VERSION.SDK_INT > android.os.Build.VERSION_CODES.JELLY_BEAN) {
            configuration.setLocale(locale);
        } else {
            configuration.locale = locale;
        }
        resources.updateConfiguration(configuration, resources.getDisplayMetrics());
    }

    @Test
    public void enterLogin_initialize_validateView(){

        setLocaleTo(Locale.JAPAN);

        Espresso.onView(ViewMatchers.withId(R.id.tv_username)).check(ViewAssertions.matches(ViewMatchers.withText(R.string.email_text)));

        Espresso.onView(ViewMatchers.withId(R.id.tv_password)).check(ViewAssertions.matches(ViewMatchers.withText(R.string.password_text)));

        Espresso.onView(ViewMatchers.withId(R.id.ed_username)).check(ViewAssertions.matches(ViewMatchers.withHint(R.string.email_hint_text)));

        Espresso.onView(ViewMatchers.withId(R.id.ed_password)).check(ViewAssertions.matches(ViewMatchers.withHint(R.string.password_hint_text)));

        Espresso.onView(ViewMatchers.withId(R.id.cb_agreement)).check(ViewAssertions.matches(not(ViewMatchers.isChecked())));

        Espresso.onView(ViewMatchers.withId(R.id.tv_agreement)).check(ViewAssertions.matches(ViewMatchers.withText(R.string.agreement_text)));

        Espresso.onView(ViewMatchers.withId(R.id.tv_register)).check(ViewAssertions.matches(ViewMatchers.withText(R.string.register_instruction_text)));

        Espresso.onView(ViewMatchers.withId(R.id.tv_forgot)).check(ViewAssertions.matches(ViewMatchers.withText(R.string.forgot_password_instruction_text)));

        Espresso.onView(ViewMatchers.withId(R.id.btn_login)).check(ViewAssertions.matches(ViewMatchers.withText(R.string.login_text)));

        Espresso.onView(ViewMatchers.withId(R.id.btn_login)).check(ViewAssertions.matches(not(ViewMatchers.isEnabled())));
    }

    @Test
    public void login_inputFull_enableLoginButton(){

        Espresso.onView(ViewMatchers.withId(R.id.ed_username)).perform(ViewActions.typeText("22222"));

        Espresso.onView(ViewMatchers.withId(R.id.ed_password)).perform(ViewActions.typeText("123456"));

        Espresso.onView(ViewMatchers.withId(R.id.cb_agreement)).perform(ViewActions.click()).check(ViewAssertions.matches(ViewMatchers.isChecked()));

        Espresso.onView(ViewMatchers.withId(R.id.btn_login)).check(ViewAssertions.matches(ViewMatchers.isEnabled()));

    }

}
