package com.fortress.di.modules;

import com.fortress.data.remote.ApiHelper;
import com.fortress.data.remote.IApiHelper;
import com.fortress.network.Api;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hieunt on 4/20/17.
 */

@Module
public class DataModule {

    @Singleton
    @Provides
    public IApiHelper provideApiHelper(Api pApi){
        return new ApiHelper(pApi);
    }

}
