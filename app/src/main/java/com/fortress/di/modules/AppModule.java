package com.fortress.di.modules;

import android.content.Context;
import android.content.res.Resources;

import com.fortress.BuildConfig;
import com.fortress.commons.App;
import com.fortress.network.NetConstants;
import com.fortress.utils.NetworkUtils;
import com.fortress.utils.StateManager;
import com.fortress.utils.StateManagerImpl;

import java.io.File;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;

/**
 * Created by hieunt on 4/5/17.
 */

@Module
public class AppModule {

    @Provides
    @Singleton
    @Named("isConnected")
    public boolean provideIsConnected(@Named("application_ctx") Context pContext) {
        return NetworkUtils.isNetworkConnected(pContext);
    }

    @Provides
    @Singleton
    @Named("cacheDir")
    public File provideCacheDir(@Named("application_ctx") Context pContext) {
        return pContext.getCacheDir();
    }

    @Provides
    @Singleton
    public HttpUrl provideEndpoint() {
        return HttpUrl.parse(BuildConfig.BASE_URL);
    }


    @Provides
    @Singleton
    public StateManager provideStateManager(StateManagerImpl pStateManagerImpl){
        return pStateManagerImpl;
    }

    @Provides
    @Singleton
    @Named("isDebug")
    public boolean provideIsDebug() {
        return BuildConfig.DEBUG;
    }

    @Provides
    @Singleton
    @Named("networkTimeoutInSeconds")
    public int provideNetworkTimeoutInSeconds() {
        return NetConstants.CONNECT_TIMEOUT;
    }

    @Provides
    @Singleton
    @Named("cacheSize")
    public long provideCacheSize() {
        return NetConstants.CACHE_SIZE;
    }

    @Provides
    @Singleton
    @Named("cacheMaxAge")
    public int provideCacheMaxAgeMinutes() {
        return NetConstants.CACHE_MAX_AGE;
    }

    @Provides
    @Singleton
    @Named("cacheMaxStale")
    public int provideCacheMaxStaleDays() {
        return NetConstants.CACHE_MAX_STALE;
    }

    @Provides
    @Singleton
    @Named("retryCount")
    public int provideApiRetryCount() {
        return NetConstants.API_RETRY_COUNT;
    }

}
