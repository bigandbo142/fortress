package com.fortress.di.modules;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.widget.Toast;

import com.fortress.commons.App;
import com.fortress.di.scopes.PerActivity;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hieunt on 4/11/17.
 */

@Module
public class AndroidModule {

    private App mApplication;

    public AndroidModule(App pApplication) {
        this.mApplication = pApplication;
    }

    @Provides
    @Singleton
    @Named("application_ctx")
    public Context provideContext(){
        return this.mApplication.getApplicationContext();
    }

    @Provides
    @Singleton
    public Resources provideResources(){
        return this.mApplication.getResources();
    }

    @Provides
    @Singleton
    public SharedPreferences provideSharePreferences(@Named("application_ctx") Context pContext){
        return pContext.getSharedPreferences("pref_file", Context.MODE_PRIVATE);
    }

    @Provides
    @Singleton
    SharedPreferences.Editor provideEditor(SharedPreferences preferences){
        return preferences.edit();
    }

}
