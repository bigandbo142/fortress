package com.fortress.di.modules;

import com.fortress.data.DataManager;
import com.fortress.di.scopes.PerActivity;
import com.fortress.features.auth.LoginActivityPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hieunt on 4/14/17.
 */

@Module
public class AuthModule {

    @Provides
    @PerActivity
    public LoginActivityPresenter provideLoginPresenter(DataManager pDataManager){
        return new LoginActivityPresenter(pDataManager);
    }

}
