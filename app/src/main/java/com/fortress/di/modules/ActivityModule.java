package com.fortress.di.modules;

import android.app.Activity;
import android.content.Context;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.fortress.di.scopes.PerActivity;
import com.fortress.widgets.CustomProgressDialog;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by hieunt on 4/14/17.
 */

@Module
public class ActivityModule {

    private Activity mActivity;

    public ActivityModule(Activity pActivity) {
        this.mActivity = pActivity;
    }

    @Provides
    @PerActivity
    public Activity provideActivity(){
        return this.mActivity;
    }

    @Provides
    @PerActivity
    @Named("activity_context")
    public Context provideContent(){
        return this.mActivity;
    }

    @Provides
    @PerActivity
    public Toast provideToast(@Named("activity_context") Context pContext){

        return Toast.makeText(pContext, "", Toast.LENGTH_SHORT);
    }

    @Provides
    @PerActivity
    public CustomProgressDialog provideProgressDialog(@Named("activity_context") Context pContext){
        return new CustomProgressDialog(pContext);
    }
}
