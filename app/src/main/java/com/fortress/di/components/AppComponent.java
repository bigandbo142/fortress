package com.fortress.di.components;

import com.fortress.commons.App;
import com.fortress.di.modules.ActivityModule;
import com.fortress.di.modules.AndroidModule;
import com.fortress.di.modules.ApiModule;
import com.fortress.di.modules.AppModule;
import com.fortress.di.modules.DataModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by hieunt on 4/5/17.
 */
@Singleton
@Component(modules = {
        AppModule.class,
        AndroidModule.class,
        ApiModule.class, // belong net-service
        DataModule.class
})
public interface AppComponent {

    ActivityComponent plus(ActivityModule pModule);

    void inject(App pApp);
}
