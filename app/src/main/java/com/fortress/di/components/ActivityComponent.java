package com.fortress.di.components;

import com.fortress.di.modules.ActivityModule;
import com.fortress.di.modules.AuthModule;
import com.fortress.di.scopes.PerActivity;
import com.fortress.features.SplashActivity;
import com.fortress.features.auth.LoginActivity;
import com.fortress.features.base.BaseActivity;

import dagger.Subcomponent;

/**
 * Created by hieunt on 4/5/17.
 */
@PerActivity
@Subcomponent(modules = {
        ActivityModule.class,
        AuthModule.class
})
public interface ActivityComponent {

    void inject (LoginActivity pActivity);
    void inject (SplashActivity pActivity);
}
