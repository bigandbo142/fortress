package com.fortress.features.base;

/**
 * Created by hieunt on 4/13/17.
 */

public interface MvpPresenter<T extends MvpView> {

    public void registerView(T activity);
    public void onAttach();
    public void onDetach();

}
