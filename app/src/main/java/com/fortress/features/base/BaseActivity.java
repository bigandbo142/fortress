package com.fortress.features.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.fortress.commons.App;
import com.fortress.di.components.ActivityComponent;
import com.fortress.di.modules.ActivityModule;
import com.fortress.widgets.CustomProgressDialog;

import javax.inject.Inject;

/**
 * Created by hieunt on 4/13/17.
 */

public abstract class BaseActivity<V extends MvpPresenter> extends AppCompatActivity implements MvpView {

    private ActivityComponent mActivityComponent;
    @Inject Toast mToast;
    @Inject CustomProgressDialog mProgressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void toggleLoading(boolean isShow) {
        if(isShow){
            mProgressDialog.show();
        }else{
            mProgressDialog.hide();
        }
    }

    @Override
    public void showToast(String pMsg) {
        mToast.setText(pMsg);
        mToast.show();
    }

    @Override
    public void hideToast() {
        if(mToast != null)
            mToast.cancel();
    }

    @Override
    protected void onPause() {
        super.onPause();
        hideToast();
    }

    public ActivityComponent getActivityComponent(){
        if(mActivityComponent == null){
            mActivityComponent = App.getComponent()
                    .plus(new ActivityModule(this));
        }

        return mActivityComponent;
    }

    protected abstract void getViewReferences();
    protected abstract void initViews();
    protected abstract V getPresenter();

}
