package com.fortress.features.base;

/**
 * Created by hieunt on 4/13/17.
 */

public interface MvpView {

    void toggleLoading(boolean isShow);
    void showToast(String pMsg);
    void hideToast();

}
