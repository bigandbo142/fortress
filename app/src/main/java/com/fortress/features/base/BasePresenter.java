package com.fortress.features.base;

/**
 * Created by hieunt on 4/13/17.
 */

public abstract class BasePresenter<V extends MvpView>  implements MvpPresenter<V>{

    private V mView;

    @Override
    public void registerView(V pView) {
        this.mView = pView;
    }

    @Override
    public void onAttach() {

    }

    @Override
    public void onDetach() {
        this.mView = null;
    }

    protected V getView(){
        return this.mView;
    }

    protected boolean isViewAttached(){
        return getView() != null;
    }
}
