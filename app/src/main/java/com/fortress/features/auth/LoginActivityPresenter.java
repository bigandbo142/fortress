package com.fortress.features.auth;

import android.support.annotation.VisibleForTesting;
import android.util.Log;
import com.fortress.data.DataManager;
import com.fortress.di.scopes.PerActivity;
import com.fortress.features.base.BasePresenter;

import javax.inject.Inject;

/**
 * Created by hieunt on 4/14/17.
 */
@PerActivity
public class LoginActivityPresenter extends BasePresenter<LoginActivityContract.LoginView> implements LoginActivityContract.LoginPresenter {

    private static final String TAG = "LoginActivityPresenter";
    private DataManager mDataMng;

    @Inject
    public LoginActivityPresenter(DataManager pDataMng) {
        this.mDataMng = pDataMng;
    }

    @VisibleForTesting
    public DataManager getDataManager() {
        return mDataMng;
    }

    @Override
    public boolean doValidateInput(String pUsername, String pPwd) {

        return getDataManager().getValidateHelper().validateUsername(pUsername)
                && getDataManager().getValidateHelper().validatePassword(pPwd);
    }

    @Override
    public void doLoginWithInput(String pUsername, String pPwd) {
        getView().toggleLoading(true);
        getDataManager().getApiHelper().doLogin(pUsername, pPwd)
                .subscribe(
                        res -> {
                            getView().toggleLoading(false);
                            getView().loginSuccess();
                        },
                        throwable -> {
                            getView().loginFailed(throwable.getMessage());
                            getView().toggleLoading(false);
                        },
                        () -> {}
                );
    }
}
