package com.fortress.features.auth;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;

import com.fortress.R;
import com.fortress.di.modules.ActivityModule;
import com.fortress.features.base.BaseActivity;
import com.fortress.features.base.MvpPresenter;

import javax.inject.Inject;

/**
 * Created by hieunt on 4/14/17.
 */

public class LoginActivity extends BaseActivity<LoginActivityContract.LoginPresenter> implements LoginActivityContract.LoginView {

    private final static String TAG = "LoginActivity";

    private EditText ed_username;
    private EditText ed_password;
    private Button btn_login;

    @Inject
    LoginActivityPresenter mPresenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        // get all references of UI elements
        getViewReferences();

        // inject pre-defined component into activity
        getActivityComponent()
                .inject(this);

        // get presenter then register current view with presenter.
        getPresenter().registerView(this);

        // do some initializations for activity
        initViews();
    }


    @Override
    protected LoginActivityContract.LoginPresenter getPresenter() {
        return mPresenter;
    }

    /**
     * get all references of UI elements
     */
    @Override
    protected void getViewReferences() {
        ed_username = (EditText) findViewById(R.id.ed_username);
        ed_password = (EditText) findViewById(R.id.ed_password);
        btn_login = (Button) findViewById(R.id.btn_login);
    }

    /**
     * do some initializations for activity
     */
    @Override
    protected void initViews() {
        btn_login.setOnClickListener(v -> getPresenter().doLoginWithInput(ed_username.getText().toString(), ed_password.getText().toString()));
    }

    @Override
    public void validateSuccess() {

    }

    @Override
    public void validateFailed(String pMsg) {

    }

    @Override
    public void loginSuccess() {
        Log.d(TAG, "Login success");
        showToast("Login success");
    }

    @Override
    public void loginFailed(String pMsg) {
        showToast(pMsg);
    }
}
