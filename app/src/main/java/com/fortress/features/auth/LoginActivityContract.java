package com.fortress.features.auth;

import com.fortress.features.base.MvpPresenter;
import com.fortress.features.base.MvpView;

import rx.Observable;

/**
 * Created by hieunt on 4/14/17.
 */

class LoginActivityContract {
    interface LoginView extends MvpView {
        void validateSuccess();
        void validateFailed(String pMsg);
        void loginSuccess();
        void loginFailed(String pMsg);
    }

    interface LoginPresenter extends MvpPresenter<LoginView>{
        boolean doValidateInput(String pUsername, String pPwd);
        void doLoginWithInput(String pUsername, String pPwd);
    }
}
