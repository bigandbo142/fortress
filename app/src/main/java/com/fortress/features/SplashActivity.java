package com.fortress.features;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.fortress.R;
import com.fortress.commons.App;
import com.fortress.di.modules.ActivityModule;

import javax.inject.Inject;
import javax.inject.Named;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        App.getComponent().plus(new ActivityModule(this)).inject(this);
    }
}
