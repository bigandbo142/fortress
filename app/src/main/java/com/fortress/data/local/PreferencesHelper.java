package com.fortress.data.local;

import android.content.SharedPreferences;

import com.fortress.BuildConfig;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by hieunt on 4/20/17.
 */

@Singleton
public class PreferencesHelper {

    public final static String PREF_KEY_USERNAME = "key_username";
    public final static String PREF_KEY_TOKEN = "key_token";

    private final SharedPreferences mPref;

    @Inject
    SharedPreferences.Editor mEditor;

    @Inject
    public PreferencesHelper(SharedPreferences preferences) {
        mPref = preferences;
    }

    public boolean clear(){
        try {
            mEditor.clear().apply();
            return true;
        }catch (Exception ex){
            return false;
        }
    }

    public SharedPreferences getPref() {
        return mPref;
    }

    public SharedPreferences.Editor getEditor(){
        return mEditor;
    }

    public boolean storeLoginSession(String pUserName, String pToken){
        try {
            mEditor.putString(PREF_KEY_USERNAME, pUserName)
                .putString(PREF_KEY_TOKEN, pToken)
                .apply();
            return true;
        }catch (Exception ex){
            return false;
        }
    }

    public Map<String, String> loadLoginSession(){
        try {
            if(!mPref.getString(PREF_KEY_USERNAME, "").isEmpty()
                    && !mPref.getString(PREF_KEY_TOKEN, "").isEmpty()){
                Map<String, String> loginSession = new HashMap<>();

                loginSession.put(PREF_KEY_USERNAME, mPref.getString(PREF_KEY_USERNAME, ""));
                loginSession.put(PREF_KEY_TOKEN, mPref.getString(PREF_KEY_TOKEN, ""));

                return loginSession;
            }
        }catch (Exception ex){
            if(BuildConfig.DEBUG)
                ex.printStackTrace();
        }

        return null;
    }
}
