package com.fortress.data.remote;

import com.fortress.network.Api;
import com.fortress.network.models.LoginResponse;

import javax.inject.Inject;
import javax.inject.Singleton;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by hieunt on 4/20/17.
 */

@Singleton
public class ApiHelper implements IApiHelper{

    private final Api mApi;

    @Inject
    public ApiHelper(Api pApi) {
        this.mApi = pApi;
    }

    public Api getApi() {
        return mApi;
    }

    @Override
    public Observable<LoginResponse> doLogin(String pUsername, String pPwd) {
        return getApi().doLogin(
                10,
                pUsername,
                pPwd,
                "abc",
                1
        );
    }
}
