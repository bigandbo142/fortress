package com.fortress.data.remote;

import com.fortress.network.models.LoginResponse;

import rx.Observable;

/**
 * Created by hieunt on 4/20/17.
 */

public interface IApiHelper {

    Observable<LoginResponse> doLogin(String pUsername, String pPwd);

}
