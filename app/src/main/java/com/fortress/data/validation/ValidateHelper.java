package com.fortress.data.validation;

import android.support.annotation.NonNull;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by hieunt on 4/19/17.
 */

@Singleton
public class ValidateHelper {

    @Inject
    public ValidateHelper() {
    }

    public boolean validateUsername(@NonNull String pUsername){
        if(pUsername != null)
            if(!pUsername.isEmpty())
                if(pUsername.length() >= 5 && pUsername.length() <= 32)
                    return true;

        return false;
    }

    public boolean validatePassword(@NonNull String pPwd){
        if(pPwd != null)
            if (!pPwd.isEmpty())
                if(pPwd.length() >= 6 && pPwd.length() <= 255)
                    return true;

        return false;
    }
}
