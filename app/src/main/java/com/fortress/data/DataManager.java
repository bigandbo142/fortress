package com.fortress.data;

import com.fortress.data.local.PreferencesHelper;
import com.fortress.data.remote.IApiHelper;
import com.fortress.data.validation.ValidateHelper;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by hieunt on 4/19/17.
 */

@Singleton
public class DataManager {
    private PreferencesHelper mPrefHelper;
    private IApiHelper mApiHelper;
    private ValidateHelper mValidateHelper;

    @Inject
    public DataManager(PreferencesHelper pPrefHelper, IApiHelper pApiHelper, ValidateHelper pValidateHelper) {
        this.mPrefHelper = pPrefHelper;
        this.mApiHelper = pApiHelper;
        this.mValidateHelper = pValidateHelper;
    }

    public PreferencesHelper getPrefHelper() {
        return mPrefHelper;
    }

    public IApiHelper getApiHelper() {
        return mApiHelper;
    }

    public ValidateHelper getValidateHelper() {
        return mValidateHelper;
    }
}
