package com.fortress.widgets;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;

import com.fortress.R;

/**
 * Created by hieunt on 4/18/17.
 */

public class CustomProgressDialog {

    private ProgressDialog mProgressDialog;
    private Context mContext;

    public CustomProgressDialog(Context mContext) {
        this.mContext = mContext;
    }

    public void show(){
        if(mProgressDialog == null){
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.show();
            if (mProgressDialog.getWindow() != null) {
                mProgressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
            mProgressDialog.setContentView(R.layout.dialog_progress);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setCancelable(true);
            mProgressDialog.setCanceledOnTouchOutside(false);
        }else{
            hide();
            mProgressDialog.show();
        }
    }

    public void hide(){
        if(mProgressDialog != null && mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }
    }
}
