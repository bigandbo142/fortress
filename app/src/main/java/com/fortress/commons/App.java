package com.fortress.commons;

import android.app.Application;

import com.fortress.di.components.AppComponent;
import com.fortress.di.components.DaggerAppComponent;
import com.fortress.di.modules.AndroidModule;

/**
 * Created by hieunt on 4/2/17.
 */

public class App extends Application {
    protected static AppComponent mComponent;

    public static AppComponent getComponent(){
        return mComponent;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mComponent = initAppComponent();

        getComponent().inject(this);
    }

    protected AppComponent initAppComponent(){
        return DaggerAppComponent.builder()
                .androidModule(new AndroidModule(this))
                .build();
    }
}
