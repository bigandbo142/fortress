package com.fortress.utils;

import android.content.Context;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * Created by hieunt on 4/11/17.
 */

public class StateManagerImpl implements StateManager {

    private Context mContext;

    @Inject
    public StateManagerImpl(@Named("application_ctx") Context pContext) {
        this.mContext = pContext;
    }

    @Override
    public boolean isConnected() {
        return NetworkUtils.isNetworkConnected(this.mContext);
    }
}
